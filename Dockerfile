# Menggunakan base image PHP dengan Apache
FROM php:8.0-apache

# Jalankan module php yang di butuhkan 
RUN apt-get update && apt-get install -y \
    libxml2-dev \
    libicu-dev \
    && docker-php-ext-install mysqli pdo pdo_mysql intl

# Jalankan dan aktifkan module rewrite
RUN a2enmod rewrite

# Copy konfigurasi webserver apache2 ke file 000-default.conf yang ada di container host
COPY apache2.conf /etc/apache2/sites-available/000-default.conf

# Jalankan workdir pada container sesuai dengan path documentroot pada webserver atau apache2
WORKDIR /var/www/html

# Copy semua file aplikasi dari host kedalam container
COPY . .

# Ubah mode writable pada semua file project
RUN chmod -R 777 writable

# Expose port container
EXPOSE 80

# Perintah untuk menjalankan Apache saat container dimulai
CMD [ "apache2-foreground" ]