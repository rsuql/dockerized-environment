# Dockerized Environment

Welcome to our Dockerized Environment project! This repository aims to streamline the development and deployment process by providing a containerized environment for your applications.

## Overview

When working on projects that require containerized environments for development and deployment, managing dependencies and configurations can become complex. This project offers a structured setup to simplify this process and ensure consistency across different environments.

## Features

- **Isolated Development Environment**: Docker allows you to encapsulate your application and its dependencies, ensuring a consistent environment across different development machines.
- **Efficient Deployment**: With Docker, deploying your application becomes more straightforward as you can package all dependencies into a single container, eliminating compatibility issues.
- **Scalability**: Docker's containerization enables easy scaling of your application by running multiple instances of containers, ensuring high availability and performance.

## Getting Started

### Prerequisites

Before you begin, ensure you have Docker installed on your local machine. If not, you can follow the installation instructions [here](https://docs.docker.com/engine/install/).

### Setup

1. Clone this repository to your local machine:

    ```bash
    git clone https://gitlab.com/rsuql/dockerized-environment.git
    ```

2. Navigate to the project directory:

    ```bash
    cd dockerized-environment
    ```

3. Build and run the Docker containers using Docker Compose:
    ```bash
    docker build -t nama_image .
    ```

    ```bash
    docker-compose up -d
    ```

4. Access your application at `http://localhost:8880` in your web browser.

## Structure Directory Project
```
## Struktur Folder Project
project_aplikasi/
│
├── app/
│   ├── Controllers/
│   ├── Models/
│   ├── Views/
│   └── ...
│
├── system/
│   └── ...
│
├── public/
│   ├── .htaccess
│   └── index.php
│
├── writable/
│   └── ...
│
├── Dockerfile
├── apache2.conf
└── docker-compose.yml
```


## Authors

- **mahendrarendi** - [@mahendrarendi](https://gitlab.com/mahendrarendi)

